package com.jiafuwei.java.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Sample {
	private int a=1; //实例变量
	public void b(){
		int a=2; //局部变量
		System.out.println("局部变量:a="+a);
		System.out.println("实例变量:a="+this.a);//局部变量的作用域内引用实例变量:this.变量名
	}
	public static void main(String[] args) throws ParseException{
		//new Sample().b();
		System.out.println(System.currentTimeMillis()/1000);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");  
		Date startDate = dateFormatter.parse("2017/2/10 14:30:00");  
		Timer timer = new Timer();  
		timer.scheduleAtFixedRate(new TimerTask(){  
		   public void run()  {  
			   try {  
                   Thread.sleep(6000);  
               } catch (InterruptedException e) {  
                   e.printStackTrace();  
               } 
		       System.out.println("execute task!" + this.scheduledExecutionTime()/1000);  
		   }  
		},startDate,5*1000); 
	}
}